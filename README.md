# Time tracker
[![codecov](https://codecov.io/gl/MRmlik12/time-tracker-python/branch/main/graph/badge.svg?token=5HMBEUHBUT)](https://codecov.io/gl/MRmlik12/time-tracker-python)
[![pipeline](https://gitlab.com/MRmlik12/time-tracker-python/badges/main/pipeline.svg)](https://gitlab.com/MRmlik12/time-tracker-python/badges/main/pipeline.svg)
## Usage
### Before running task
Create file .env in root directory of project with content
```
TOGGL_API_KEY=YOUR_TOKEN
```
Install dependencies from requirements.txt
```bash
$ python3 -m pip install -r requirements.txt 
```

### Create task
START_DATE, END_DATE - "dd.mm.yyyy hh:mm"
```bash
$ python3 ./tracker/main.py create-task --project-id=PROJECT_ID --project-name=PROJECT_NAME --description=DESCRIPTION --start=START_DATE --endEND_DATE
```

### Create tasks
DATE - "hh:mm-hh:mm, dd.mm.yyyy, |0(Sunday)-1-2-3-4-5-6(Saturday)|"
```bash
$ python3 ./tracker/main.py create-task --project-id=PROJECT_ID --project-name=PROJECT_NAME --description=DESCRIPTION --date=DATE
```

### Start task
```bash
$ python3 ./tracker/main.py start-task --description=DESCRIPTION --project-id=PROJECT_ID
```

### End task
```bash
$ python3 ./tracker/main.py end-task --task-id=TASK_ID
```