import datetime

from click import secho
from typer import Typer, Option, colors
from tracker.api import Api
from tracker.utils import parse_datetime, datetime_to_string

app = Typer()
api = Api()


@app.command()
def create_task(
    project_id: int = Option(0, help="Project id"),
    project_name: str = Option(
        "Example project", help='Project name ex. "Example project"'
    ),
    description: str = Option(
        "Example task", help='Description of task ex. "Example task"'
    ),
    start: str = Option(
        "",
        help='Set when task was started with format "dd.mm.yyyy hh:mm" for example '
        '"20.04.2022 14:00"',
    ),
    end: str = Option(
        "",
        help='Set when task was end with format "dd.mm.yyyy hh:mm" for example "22.04.2022 15:00"',
    ),
):
    datetime_format = "%d.%m.%Y %H:%M"
    start_datetime = datetime.datetime.strptime(start, datetime_format)
    end_datetime = datetime.datetime.strptime(end, datetime_format)
    response = api.create_time_entry(
        project_name, project_id, description, start_datetime, end_datetime
    )
    task_id = response["data"]["id"]
    secho(f"Task with id={task_id} was created!", fg=colors.GREEN)


def get_days(
    start: datetime.datetime,
    end: datetime.datetime,
    hours: list[str],
    days_mask: list[str],
    pattern: str,
) -> list[list[datetime.datetime]]:
    days: list[list[datetime.datetime]] = []

    while start <= end:
        if start.strftime("%w") in days_mask:
            start_at = parse_datetime(
                hours[0] + " " + datetime_to_string(start, "%d.%m.%Y"), pattern
            )
            end_at = parse_datetime(
                hours[1] + " " + datetime_to_string(start, "%d.%m.%Y"), pattern
            )
            days.append([start_at, end_at])
        start += datetime.timedelta(days=1)

    return days


@app.command()
def create_tasks(
    project_id: int = Option(0, help="Project id"),
    project_name: str = Option("", help='Project name ex. "Example project"'),
    description: str = Option("", help='Description of task ex. "Example task"'),
    date: str = Option(
        "",
        help="Set multiple tasks by typing time, date range and days, "
        '"hh:mm-hh:mm, dd.mm.yyyy, |0(Sunday)-1-2-3-4-5-6(Saturday)|" '
        'for example "10:00-12:00, 15.05.2022-30.05.2022, |0-3| "'
        "will be set task from 10:00 to 12:00 in Sunday and Wednesday"
        " with date range 15.05.2022 to 30.05.2022",
    ),
):
    datetime_format = "%H:%M %d.%m.%Y"
    split_date = date.split(",")
    split_dates = split_date[1].split("-")
    days_mask = split_date[2].split("|")[1].split("-")
    start = parse_datetime(split_dates[0].strip(), "%d.%m.%Y")
    end = parse_datetime(split_dates[1].strip(), "%d.%m.%Y")
    hours = split_date[0].split("-")

    days = get_days(start, end, hours, days_mask, datetime_format)

    for day in days:
        response = api.create_time_entry(
            project_name, project_id, description, day[0], day[1]
        )
        task_id = response["data"]["id"]
        secho(f"Task with id={task_id} was created!", fg=colors.GREEN)


@app.command()
def start_task(
    description: str = Option("", help='Description of task ex. "Example task"'),
    project_id: int = Option(0, help="Project id ex. 839123443"),
):
    response = api.start_time_entry(description, project_id)
    task_id = response["data"]["id"]
    secho(f"Task with id={task_id} was started!", fg=colors.GREEN)


@app.command()
def end_task(task_id: int = Option(0, help="Task id from start-task command")):
    response = api.stop_time_entry(task_id)
    name = response["data"]["description"]
    secho(f"Task with title {name} was ended!", fg=colors.GREEN)


if __name__ == "__main__":
    app()
