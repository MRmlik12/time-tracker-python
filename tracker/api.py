import datetime

from toggl.TogglPy import Toggl

from tracker.constants import TOGGL_API_KEY


class Api:
    def __init__(self):
        self.toggl = Toggl()
        self.toggl.setAPIKey(TOGGL_API_KEY)

    def create_time_entry(
        self,
        project_name: str,
        project_id: int,
        description: str,
        start: datetime.datetime,
        end: datetime.datetime,
    ):
        duration = int((end - start).total_seconds() / 3600)

        return self.toggl.createTimeEntry(
            hourduration=duration,
            projectid=project_id,
            description=description,
            projectname=project_name,
            year=start.year,
            month=start.month,
            day=start.day,
            hour=start.hour,
        )

    def stop_time_entry(self, entry_id: int):
        return self.toggl.request(
            f"https://api.track.toggl.com/api/v8/time_entries/{entry_id}/stop"
        )

    def start_time_entry(self, description: str, project_id: int):
        return self.toggl.startTimeEntry(description, project_id)
