import os
from typing import Optional

from dotenv import load_dotenv

load_dotenv()

TOGGL_API_KEY: Optional[str] = os.getenv("TOGGL_API_KEY")
