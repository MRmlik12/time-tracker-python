import datetime


def parse_datetime(date_string: str, pattern: str) -> datetime.datetime:
    return datetime.datetime.strptime(date_string, pattern)


def datetime_to_string(date: datetime.datetime, pattern: str) -> str:
    return date.strftime(pattern)
