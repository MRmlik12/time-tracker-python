import datetime
import toggl.TogglPy
from pytest_mock import MockerFixture

from tracker.api import Api

mocked_value = {"data": {"id": 233, "description": "Test"}}


def test_create_time_entry(mocker: MockerFixture):
    mocker.patch.object(toggl.TogglPy.Toggl, "setAPIKey", autospec=True)
    mocker.patch.object(
        toggl.TogglPy.Toggl, "createTimeEntry", autospec=True, return_value=mocked_value
    )
    api = Api()
    result = api.create_time_entry(
        "test", 2222, "Test", datetime.datetime.now(), datetime.datetime.now()
    )

    assert result == mocked_value


def test_start_time_entry(mocker: MockerFixture):
    mocker.patch.object(toggl.TogglPy.Toggl, "setAPIKey", autospec=True)
    mocker.patch.object(
        toggl.TogglPy.Toggl, "startTimeEntry", autospec=True, return_value=mocked_value
    )
    api = Api()
    result = api.start_time_entry("Test", 233)

    assert result == mocked_value


def test_stop_time_entry(mocker: MockerFixture):
    mocker.patch.object(toggl.TogglPy.Toggl, "setAPIKey", autospec=True)
    mocker.patch.object(
        toggl.TogglPy.Toggl, "request", autospec=True, return_value=mocked_value
    )
    api = Api()
    result = api.stop_time_entry(233)

    assert result == mocked_value
