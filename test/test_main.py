import toggl
from typer.testing import CliRunner
from pytest_mock import MockFixture

import tracker.api
import tracker.main

runner = CliRunner()
mocked_value = {"data": {"id": 443, "description": "Test"}}


def test_create_task(mocker: MockFixture):
    mocker.patch.object(toggl.TogglPy.Toggl, "setAPIKey", autospec=True)
    mocker.patch.object(
        tracker.api.Api, "create_time_entry", autospec=True, return_value=mocked_value
    )
    result = runner.invoke(
        tracker.main.app,
        [
            "create-task",
            "--project-id=442",
            "--project-name=test",
            "--description=Test",
            "--start=22.05.2022 14:00",
            "--end=22.05.2022 15:00",
        ],
    )
    assert result.exit_code == 0
    assert "Task with id=443 was created!" in result.stdout


def test_create_tasks(mocker: MockFixture):
    mocker.patch.object(toggl.TogglPy.Toggl, "setAPIKey", autospec=True)
    mocker.patch.object(
        tracker.api.Api, "create_time_entry", autospec=True, return_value=mocked_value
    )
    result = runner.invoke(
        tracker.main.app,
        [
            "create-tasks",
            "--project-id=442",
            "--project-name=test",
            "--description=Test",
            "--date=10:00-12:00, 15.05.2022-30.05.2022, |0-6|",
        ],
    )
    assert result.exit_code == 0
    assert "Task with id=443 was created!" in result.stdout


def test_start_task(mocker: MockFixture):
    mocker.patch.object(toggl.TogglPy.Toggl, "setAPIKey", autospec=True)
    mocker.patch.object(
        tracker.api.Api, "start_time_entry", autospec=True, return_value=mocked_value
    )
    result = runner.invoke(
        tracker.main.app, ["start-task", "--description=Testing task", "--project-id=181905963"]
    )
    assert result.exit_code == 0
    assert "Task with id=443 was started!" in result.stdout


def test_end_task(mocker: MockFixture):
    mocker.patch.object(toggl.TogglPy.Toggl, "setAPIKey", autospec=True)
    mocker.patch.object(
        tracker.api.Api, "stop_time_entry", autospec=True, return_value=mocked_value
    )
    result = runner.invoke(tracker.main.app, ["end-task", "--task-id=443"])
    assert result.exit_code == 0
    assert "Task with title Test was ended!" in result.stdout
