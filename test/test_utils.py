import datetime
from tracker.utils import parse_datetime, datetime_to_string


def test_datetime_to_string_util():
    expected = "20.04.2022"
    date = datetime.datetime(2022, 4, 20)
    result = datetime_to_string(date, "%d.%m.%Y")
    assert result == expected


def test_parse_datetime_util():
    expected = datetime.datetime(2022, 4, 20)
    result = parse_datetime("20.04.2022", "%d.%m.%Y")

    assert result == expected
